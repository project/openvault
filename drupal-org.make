api = 2
core = 7.x

; A make file to specify OpenVault distro

; Modules
projects[ctools][subdir] = "contrib"

projects[features][subdir] = "contrib"

projects[date][subdir] = "contrib"

projects[email][subdir] = "contrib"

projects[entity][subdir] = "contrib"

projects[entityreference][subdir] = "contrib"

projects[field_collection][subdir] = "contrib"

projects[field_group][subdir] = "contrib"

projects[link][subdir] = "contrib"

projects[field_secret][subdir] = "contrib"

projects[messaging][subdir] = "contrib"

projects[aes][subdir] = "contrib"

projects[zeroclipboard][subdir] = "contrib"
projects[zeroclipboard][patch][1951422] = "http://drupal.org/files/zeroclipboard-fix-for-new-js-library-1951422-0.patch"

projects[backup_migrate][subdir] = "contrib"

projects[libraries][subdir] = "contrib"

projects[pathauto][subdir] = "contrib"

projects[token][subdir] = "contrib"

projects[transliteration][subdir] = "contrib"

projects[panels][subdir] = "contrib"

projects[views][subdir] = "contrib"

projects[rules][subdir] = "contrib"

projects[context][subdir] = "contrib"

projects[rules][subdir] = "contrib"

projects[strongarm][subdir] = "contrib"

projects[jquery_update][subdir] = "contrib"

; Themes
projects[bootstrap][subdir] = "contrib"
projects[bootstrap][version] = "2.x-dev"
