<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_install_tasks()
 */
function openvault_install_tasks(&$install_state) {
  // Add our custom CSS file for the installation process
  drupal_add_css(drupal_get_path('profile', 'openvault') . '/openvault.css');
}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function openvault_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_block_view().
 */
function openvault_block_view($delta = '') {
  switch ($delta) {
    case 'powered-by':
      return array(
        'subject' => NULL,
        'content' => '<span>' . variable_get('site_name', t('This site')) . ' ' . t('is powered by <a href="http://drupal.org/project/openvault" title="OpenVault" target="_blank">!openvault</a>. A distribution by <a href="http://www.vardot.com" title="Vardot.com" target="_blank">!vardot</a>.', array('!openvault' => 'OpenVault', '!vardot' => 'Vardot.com')) . '</span>',
      );
  }
}


/**
 * Implements hook_block_info()
 */
function openvault_block_info() {
  $blocks['powered-by'] = array(
    'info' => t('Powered by OpenVault'),
    'weight' => '20',
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}