<?php

function openvault_notify_override_settings_modal($nid, $plan_id) {
  // Include the CTools tools that we need.
  ctools_include('ajax');
  ctools_include('modal');

  $form_state['ajax'] = TRUE;
  $form_state['build_info']['args'] = array($nid, $plan_id);

  //form_load_include($form_state, 'inc', 'node', 'node.pages');

  $output = ctools_modal_form_wrapper('openvault_notify_settings_form', $form_state);
  if (!empty($form_state['executed'])) {
    // We'll just overwrite the form output if it was successful.
    $output = array();
    ctools_add_js('ajax-responder');
    $output[] = ctools_modal_command_dismiss(t('Success!'));
    if (isset($_GET['destination'])) {
      $output[] = ctools_ajax_command_redirect($_GET['destination']);
    }
    else {
      $output[] = ctools_ajax_command_reload();
    }
  }

  print ajax_render($output);
  exit;
}

function openvault_notify_settings_form($form, $form_state, $nid = NULL, $plan_id = NULL) {
  $form = array();

  $form['override_nid'] = array(
    '#type'  => 'value',
    '#value' => $nid,
  );

  if ($nid) {
    $form['website_name'] = array(
      '#type'  => 'value',
      '#value' => node_load($nid)->title,
    );
  }

  $form['override_plan_id'] = array(
    '#type'  => 'value',
    '#value' => $plan_id,
  );

  if ($plan_id) {
    $form['plan_name'] = array(
      '#type'  => 'value',
      '#value' => field_collection_item_load($plan_id)->field_plan_label['und'][0]['value'],
    );
  }

  if ($override_values = db_select('openvault_notify_settings_overrides', 'ovs')
                           ->fields('ovs')
                           ->condition('website_nid', $nid, '=')
                           ->condition('plan_id', $plan_id, '=')
                           ->execute()
                           ->fetchAssoc()) {
    $form_values = array(
      'notification_message'    => $override_values['message'],
      'notification_recipients' => unserialize($override_values['preset_recipients']),
      'other_recipients'        => unserialize($override_values['other_recipients']),
      'periods'                 => unserialize($override_values['periods']),
    );
  }
  else {
    $form_values = array(
      'notification_message'    => variable_get('openvault_notify_notification_mesasge', OPENVAULT_NOTIFY_NOTIFICATION_MESSAGE),
      'notification_recipients' => variable_get('openvault_notify_notification_recipients', array()),
      'other_recipients'        => variable_get('openvault_notify_recipients', array()),
      'periods'                 => variable_get('openvault_notify_periods', array()),
    );
  }

  $form['notification_message'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Notification Message Text'),
    '#required'      => TRUE,
    '#default_value' => $form_values['notification_message'],
    '#rows'          => 10,
    '#description'   => t('Available tokens: %plan_name, %website_name, %period_left, %expiration_date'),
  );

  $form['notification_recipients'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('To'),
    '#options'       => _openvault_notify_emails(),
    '#default_value' => $form_values['notification_recipients'],
  );

  $form['other_recipients'] = array('#tree' => TRUE);

  $form['recipient_delta'] = array(
    '#type'          => 'value',
    '#value'         => 1,
  );

  // Build the recipients form elements
  if (isset($form_state['triggering_element']['#id']) && $form_state['triggering_element']['#id'] == 'edit-another-recipient--2') {
    for ($i = 0; $i <= $form_state['values']['recipient_delta']; $i++) {
      $form['other_recipients']['other_recipients_' . $i] = array(
        '#type'        => 'textfield',
        '#title'       => t('Another Recipient E-Mail'),
        '#description' => t('Add more e-mail addresses, one on each line.'),
      );
    }

    $form['recipient_delta']['#value'] = $form_state['values']['recipient_delta'] + 1;
  }
  else {
    if (!isset($form_state['triggering_element']['#id'])) {
      $current_recipients = $form_values['other_recipients'];
    }
    else {
      $current_recipients = $form_state['values']['other_recipients'];
    }
    $recipient_delta = 0;

    foreach ($current_recipients as $current_recipient) {
      $form['other_recipients']['other_recipients_' . $recipient_delta] = array(
        '#type'          => 'textfield',
        '#title'         => t('Another Recipient E-Mail'),
        '#default_value' => $current_recipient,
      );

      $recipient_delta++;
    }

    if (!isset($form_state['triggering_element']['#id'])) {
      $form['other_recipients']['other_recipients_' . $recipient_delta] = array(
        '#type'          => 'textfield',
        '#title'         => t('Another Recipient E-Mail'),
      );
    }

    $form['recipient_delta']['#value'] = $recipient_delta + 1;
  }

  $form['new_other_recipient_wrapper'] = array(
    '#type'          => 'markup',
    '#markup'        => '<div id="new-other-recipient-wrapper"></div>',
  );

  $form['another_recipient'] = array(
    '#type'          => 'button',
    '#value'         => t('Add Another E-Mail'),
    '#ajax'          => array(
      'callback' => 'openvault_notify_settings_form__another_recipient__ajax',
      'wrapper'  => 'new-other-recipient-wrapper',
      'method'   => 'before',
      'effect'   => 'fade',
    ),
  );

  $form['periods_fieldset'] = array(
    '#type'          => 'fieldset',
    '#collapsible'   => FALSE,
    '#collapsed'     => FALSE,
    '#attributes'    => array('class' => array('container-inline')),
    '#tree'          => TRUE,
  );

  $form['periods_fieldset']['periods'] = array('#tree' => TRUE);

  $form['period_delta'] = array(
    '#type'          => 'value',
    '#value'         => 1,
  );

  if (isset($form_state['triggering_element']['#id']) && $form_state['triggering_element']['#id'] == 'edit-periods-fieldset-another-period--2') {
    for ($i = 0; $i <= $form_state['values']['period_delta']; $i++) {
      $form['periods_fieldset']['periods']['period_' . $i] = _openvault_notify_period_form_element();
    }

    $form['period_delta']['#value'] = $form_state['values']['period_delta'] + 1;
  }
  else {
    if (!isset($form_state['triggering_element']['#id'])) {
      $current_periods = $form_values['periods'];
    }
    else {
      $current_periods = $form_state['values']['periods_fieldset']['periods'];
    }
    $periods_delta = 0;

    foreach ($current_periods as $current_period) {
      $form['periods_fieldset']['periods']['period_' . $periods_delta] = _openvault_notify_period_form_element();

      // Set the default values
      $form['periods_fieldset']['periods']['period_' . $periods_delta]['number']['#default_value'] = $current_period['number'];
      $form['periods_fieldset']['periods']['period_' . $periods_delta]['unit']['#default_value'] = $current_period['unit'];

      $periods_delta++;
    }

    if (!isset($form_state['triggering_element']['#id'])) {
      $form['periods_fieldset']['periods']['period_' . $periods_delta] = _openvault_notify_period_form_element();
    }

    $form['period_delta']['#value'] = $periods_delta + 1;
  }

  $form['periods_fieldset']['new_period_wrapper'] = array(
    '#type'          => 'markup',
    '#markup'        => '<div id="new-period-wrapper"></div>',
  );

  $form['periods_fieldset']['another_period'] = array(
    '#type'          => 'button',
    '#value'         => t('Add Another Period'),
    '#ajax'          => array(
      'callback' => 'openvault_notify_settings_form__another_period__ajax',
      'wrapper'  => 'new-period-wrapper',
      'method'   => 'before',
      'effect'   => 'fade',
    ),
  );

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Save'),
    '#submit' => array('openvault_notify_settings_form_save_submit'),
  );

  return $form;
}

function openvault_notify_settings_form__another_recipient__ajax($form, $form_state) {
  return $form['other_recipients']['other_recipients_' . $form_state['values']['recipient_delta']];
}

function openvault_notify_settings_form__another_period__ajax($form, $form_state) {
  return $form['periods_fieldset']['periods']['period_' . $form_state['values']['period_delta']];
}

function openvault_notify_settings_form_save_submit($form, $form_state) {
  foreach ($form_state['values']['notification_recipients'] as $index => $predefined_recipient) {
    if ($predefined_recipient === 0) {
      unset($form_state['values']['notification_recipients'][$index]);
    }
  }

  // Save the emails
  $recipients_to_save = array();
  if (isset($form_state['values']['other_recipients'])) {
    foreach ($form_state['values']['other_recipients'] as $recipient) {
      if ($recipient) {
        $recipients_to_save[] = $recipient;
      }
    }
  }

  // Save the periods
  $periods_to_save = array();
  foreach ($form_state['values']['periods_fieldset']['periods'] as $period) {
    if ($period['number'] > 0) {
      $periods_to_save[] = $period;
    }
  }

  // Save all settings here, either as global, or as overrides
  if (is_null($form_state['values']['override_nid'])) {
    variable_set('openvault_notify_notification_mesasge', $form_state['values']['notification_message']);
    variable_set('openvault_notify_notification_recipients', $form_state['values']['notification_recipients']);
    variable_set('openvault_notify_recipients', $recipients_to_save);
    variable_set('openvault_notify_periods', $periods_to_save);

    drupal_set_message(t('OpenVault notifications settings have been saved.'));

    $notifications_rebuild_target = 'all';
  }
  else {
    db_delete('openvault_notify_settings_overrides')
      ->condition('website_nid', $form_state['values']['override_nid'], '=')
      ->condition('plan_id', $form_state['values']['override_plan_id'], '=')
      ->execute();

    db_insert('openvault_notify_settings_overrides')
      ->fields(array(
        'website_nid'       => $form_state['values']['override_nid'],
        'plan_id'           => $form_state['values']['override_plan_id'],
        'message'           => $form_state['values']['notification_message'],
        'preset_recipients' => serialize($form_state['values']['notification_recipients']),
        'other_recipients'  => serialize($recipients_to_save),
        'periods'           => serialize($periods_to_save)))
      ->execute();

    drupal_set_message(t('OpenVault notifications settings overrides for %plan_name plan in the %website_name website have been saved.', array('%plan_name' => $form_state['values']['plan_name'], '%website_name' => $form_state['values']['website_name'])));

    $notifications_rebuild_target = $form_state['values']['override_nid'];
  }


  openvault_notify_rebuild_notifications($notifications_rebuild_target);
}

function _openvault_notify_period_form_element() {
  $element = array();

  $element['#tree'] = TRUE;

  $period_number_options = array(0 => '-');
  for ($i = 1; $i <=30; $i++) {
    $period_number_options[$i] = $i;
  }

  $period_unit_options = array(1 => t('days'), 2 => t('weeks'), 3 => t('months'));

  $element['number'] = array(
    '#type'          => 'select',
    '#title'  => t('Period'),
    '#options'       => $period_number_options,
  );

  $element['unit'] = array(
    '#type'          => 'select',
    '#attributes'    => array('class' => array('container-inline')),
    '#options'       => $period_unit_options,
    '#suffix'        => '<div style="display:block;"></div>',
  );

  return $element;
}