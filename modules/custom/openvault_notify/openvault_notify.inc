<?php

/**
 * Look at openvault_notify_rebuild_notifications().
 */
function _openvault_notify_rebuild_notifications($target) {
  // Now update all the openvault Tasks in the system
  $websites_result = db_select('node', 'n')
  ->fields('n')
  ->condition('type', 'website', '=');

  // Add the condition of the specified target
  if (is_numeric($target)) {
    $websites_result = $websites_result->condition('nid', $target, '=');
  }

  // And execute
  $websites_result = $websites_result->execute();

  // Load the default settings
  $default_periods_settings = variable_get('openvault_notify_periods', array());
  $default_recipient_emails = variable_get('openvault_notify_recipients', array());
  $default_templatic_recipients = variable_get('openvault_notify_notification_recipients', array());
  $default_notification_message = variable_get('openvault_notify_notification_mesasge', OPENVAULT_NOTIFY_NOTIFICATION_MESSAGE);

  $new_notifications = array();
  while ($website_row = $websites_result->fetchAssoc()) {
    $website_node = node_load($website_row['nid']);

    db_delete('openvault_notify_tasks')->condition('node_id', $website_node->nid, '=')->execute();

    if (is_array(field_get_items('node', $website_node, 'field_website_plans'))) {
      foreach (field_get_items('node', $website_node, 'field_website_plans') as $plan) {
        $plan_entity = field_collection_item_load($plan['value']);
        $expiration_date = $plan_entity->field_plan_duration['und'][0]['value2'];

        $overriding = FALSE;
        if (isset($plan_entity->field_plan_notification_settings['und'][0]['value'])) {
          // Check if plan is set NOT to send any notifications, simply continue
          if ($plan_entity->field_plan_notification_settings['und'][0]['value'] == '1') {
            continue;
          }

          // Check if plan is set to override default notificaitons settings, load the override settings
          if ($plan_entity->field_plan_notification_settings['und'][0]['value'] == '2' &&
              $override_settings = db_select('openvault_notify_settings_overrides', 'ovs')
                                     ->fields('ovs')
                                     ->condition('website_nid', $website_row['nid'], '=')
                                     ->condition('plan_id', $plan['value'], '=')
                                     ->execute()
                                     ->fetchAssoc()) {
            $periods_settings_to_use = unserialize($override_settings['periods']);
            $recipient_emails_to_use = unserialize($override_settings['other_recipients']);
            $notification_message_to_use = $override_settings['message'];
            $templatic_recipients_to_use = unserialize($override_settings['preset_recipients']);

            $overriding = TRUE;
          }
        }

        // If not overriding, make sure we're using the default settings
        if (!$overriding) {
          $periods_settings_to_use = $default_periods_settings;
          $recipient_emails_to_use = $default_recipient_emails;
          $notification_message_to_use = $default_notification_message;
          $templatic_recipients_to_use = $default_templatic_recipients;
        }

        // Iterate through the periods chosen
        foreach ($periods_settings_to_use as $period_to_save) {
          $multiplier = 60 * 60 * 24; // Let's start with the number of seconds in a day
          if ($period_to_save['unit'] == 1) {
            // $multiplier doesn't need to change
          }
          elseif ($period_to_save['unit'] == 2) {
            $multiplier *= 7;
          }
          elseif ($period_to_save['unit'] == 3) {
            $multiplier *= 30;
          }

          // Iterate through the chosen email addresses
          foreach ($recipient_emails_to_use as $recipient_email) {
            $new_notifications[$website_node->nid]['email'][] = array(
              'status'         => OPENVAULT_NOTIFY_TASK_NOT_DUE,
              'recipient'      => $recipient_email,
              'recipient_type' => 'email_address',
              'plan_id'        => $plan['value'],
              'message'        => $notification_message_to_use,
              'due_date'       => $expiration_date - $multiplier * $period_to_save['number'],
            );
          }
          // Notification to the site admin
          if (isset($templatic_recipients_to_use[0])) {
            $new_notifications[$website_node->nid]['email'][] = array(
              'status'         => OPENVAULT_NOTIFY_TASK_NOT_DUE,
              'recipient'      => 1,
              'recipient_type' => 'uid',
              'plan_id'        => $plan['value'],
              'message'        => $notification_message_to_use,
              'due_date'       => $expiration_date - $multiplier * $period_to_save['number'],
            );
          }
          // Notification to the client of the website
          if (isset($templatic_recipients_to_use[1])) {
            $new_notifications[$website_node->nid]['email'][] = array(
              'status'         => OPENVAULT_NOTIFY_TASK_NOT_DUE,
              'recipient'      => $website_node->field_website_client['und'][0]['target_id'],
              'recipient_type' => 'client_nid',
              'plan_id'        => $plan['value'],
              'message'        => $notification_message_to_use,
              'due_date'       => $expiration_date - $multiplier * $period_to_save['number'],
            );
          }
          // Notification to the author of the website
          if (isset($templatic_recipients_to_use[2])) {
            $new_notifications[$website_node->nid]['email'][] = array(
              'status'         => OPENVAULT_NOTIFY_TASK_NOT_DUE,
              'recipient'      => $website_node->uid,
              'recipient_type' => 'uid',
              'plan_id'        => $plan['value'],
              'message'        => $notification_message_to_use,
              'due_date'       => $expiration_date - $multiplier * $period_to_save['number'],
            );
          }
        }
      }
    }
  }

  foreach ($new_notifications as $website_nid => $website_notifications) {
    foreach ($website_notifications as $notification_type => $notifications) {
      $previous_notifications = array();
      foreach ($notifications as $notification) {
        // Create the "notification ID" to check later if this notification has already been created before
        $notification_id = array($notification['recipient'],
        $notification['recipient_type'],
        $notification['due_date']);
        if (in_array($notification_id, $previous_notifications)) {
          // Notification has already been created before, we'll just skip it
          continue;
        }
        else {
          // Add the notification ID to the list, to be able to match against it in later iterations
          $previous_notifications[] = $notification_id;
        }

        if ($notification['due_date'] > time()) {
          db_insert('openvault_notify_tasks')
          ->fields(array(
            'type'           => $notification_type,
            'status'         => $notification['status'],
            'recipient'      => $notification['recipient'],
            'recipient_type' => $notification['recipient_type'],
            'node_id'        => $website_nid,
            'plan_id'        => $notification['plan_id'],
            'message'        => $notification['message'],
            'due_date'       => $notification['due_date']))
          ->execute();
        }
      }
    }
  }
}