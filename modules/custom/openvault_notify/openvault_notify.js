/**
   * Provide the HTML to create the modal dialog.
   */
  Drupal.theme.prototype.openvaultNotifyOverrideSettings = function () {
    var html = '';

    html += '<div id="ctools-modal" class="popups-box">';
    html += '  <div class="ctools-modal-content modal-forms-modal-content">';
    html += '    <div class="popups-container">';
    html += '      <div class="modal-header popups-title clearfix">';
    html += '        <h3 id="modal-title" class="modal-title"></h3>';
    html += '        <span class="popups-close close">' + Drupal.CTools.Modal.currentSettings.closeText + '</span>';
    html += '      </div>';
    html += '      <div class="modal-scroll"><div id="modal-content" class="modal-content popups-body"></div></div>';
    html += '    </div>';
    html += '  </div>';
    html += '</div>';

    return html;
  };
(function ($) {
  

  Drupal.behaviors.openvaultNotifyForm = {
    attach: function(context, settings) {
      $('.field-name-field-plan-notification-settings select', context).change(function() {
        if ($(this).val() == '2') {
          $('span[rel=' + $(this).attr('id') + ']').show();
        }
        else {
          $('span[rel=' + $(this).attr('id') + ']').hide();
        }
      });
      
      $('.field-name-field-plan-notification-settings select', context).each(function() {
        if ($(this).val() == '2') {
          $('span[rel=' + $(this).attr('id') + ']').show();
        }
        else {
          $('span[rel=' + $(this).attr('id') + ']').hide();
        }
      });
    }
  };
})(jQuery);