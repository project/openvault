(function ($) {
  Drupal.behaviors.openvaultCredentials = {
    attach: function(context, settings) {
      $('#openvault-credentials--credential-types-displayer input').change(function() {
        if (this.checked) {
          $('#field-' + $(this).val().substr(6) + '-values').slideDown({'duration' : 'slow', 'queue': false});
          $('#field-' + $(this).val().substr(6) + '-values').fadeIn({'duration' : 'slow', 'queue': false});
        }
        else {
          $('#field-' + $(this).val().substr(6) + '-values').slideUp({'duration' : 'slow', 'queue': false});
          $('#field-' + $(this).val().substr(6) + '-values').fadeOut({'duration' : 'slow', 'queue': false});
        }
      });
    }
  };
})(jQuery);