<?php
/**
 * @file
 * openvault_entries.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openvault_entries_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function openvault_entries_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function openvault_entries_node_info() {
  $items = array(
    'client' => array(
      'name' => t('Client'),
      'base' => 'node_content',
      'description' => t('Use <em>client</em> for your data about specific clients.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'website' => array(
      'name' => t('Website'),
      'base' => 'node_content',
      'description' => t('Use <em>website</em> for your data about websites and projects.'),
      'has_title' => '1',
      'title_label' => t('Site Name'),
      'help' => '',
    ),
  );
  return $items;
}
