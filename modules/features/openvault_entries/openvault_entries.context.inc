<?php
/**
 * @file
 * openvault_entries.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function openvault_entries_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'clients_websites';
  $context->description = 'Have Clients Websites Breadcrumbs in the right way.';
  $context->tag = 'theme';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'website' => 'website',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'breadcrumb' => 'websites',
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Have Clients Websites Breadcrumbs in the right way.');
  t('theme');
  $export['clients_websites'] = $context;

  return $export;
}
