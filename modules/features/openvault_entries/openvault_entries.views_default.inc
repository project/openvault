<?php
/**
 * @file
 * openvault_entries.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function openvault_entries_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'clients';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Clients';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_website_client',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No access to any websites.';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<h3>You have no access to any websites.</h3>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Relationship: Content: Access (field_website_accessors) */
  $handler->display->display_options['relationships']['field_website_accessors_value']['id'] = 'field_website_accessors_value';
  $handler->display->display_options['relationships']['field_website_accessors_value']['table'] = 'field_data_field_website_accessors';
  $handler->display->display_options['relationships']['field_website_accessors_value']['field'] = 'field_website_accessors_value';
  $handler->display->display_options['relationships']['field_website_accessors_value']['delta'] = '-1';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '[title]<i class="icon-chevron-right"></i>';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Client */
  $handler->display->display_options['fields']['field_website_client']['id'] = 'field_website_client';
  $handler->display->display_options['fields']['field_website_client']['table'] = 'field_data_field_website_client';
  $handler->display->display_options['fields']['field_website_client']['field'] = 'field_website_client';
  $handler->display->display_options['fields']['field_website_client']['label'] = '';
  $handler->display->display_options['fields']['field_website_client']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_website_client']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_website_client']['settings'] = array(
    'link' => 0,
  );
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['sorts']['title']['order'] = 'DESC';
  /* Sort criterion: Content: Client (field_website_client) */
  $handler->display->display_options['sorts']['field_website_client_target_id']['id'] = 'field_website_client_target_id';
  $handler->display->display_options['sorts']['field_website_client_target_id']['table'] = 'field_data_field_website_client';
  $handler->display->display_options['sorts']['field_website_client_target_id']['field'] = 'field_website_client_target_id';
  $handler->display->display_options['sorts']['field_website_client_target_id']['expose']['label'] = 'Client (field_website_client)';
  /* Contextual filter: Field collection item: User (field_accessor_user) */
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['id'] = 'field_accessor_user_target_id';
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['table'] = 'field_data_field_accessor_user';
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['field'] = 'field_accessor_user_target_id';
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['relationship'] = 'field_website_accessors_value';
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_accessor_user_target_id']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Field collection item: Access (field_accessor_access:delta) */
  $handler->display->display_options['arguments']['delta']['id'] = 'delta';
  $handler->display->display_options['arguments']['delta']['table'] = 'field_data_field_accessor_access';
  $handler->display->display_options['arguments']['delta']['field'] = 'delta';
  $handler->display->display_options['arguments']['delta']['relationship'] = 'field_website_accessors_value';
  $handler->display->display_options['arguments']['delta']['default_action'] = 'default';
  $handler->display->display_options['arguments']['delta']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['delta']['default_argument_options']['argument'] = '1,2,3';
  $handler->display->display_options['arguments']['delta']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['delta']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['delta']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'website' => 'website',
  );

  /* Display: Clients Webistes Page */
  $handler = $view->new_display('page', 'Clients Webistes Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_website_client',
      'rendered' => 0,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'field_website_logo' => 'field_website_logo',
    'title' => 'title',
    'field_website_client' => 'field_website_client',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_website_logo' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_website_client' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Logo */
  $handler->display->display_options['fields']['field_website_logo']['id'] = 'field_website_logo';
  $handler->display->display_options['fields']['field_website_logo']['table'] = 'field_data_field_website_logo';
  $handler->display->display_options['fields']['field_website_logo']['field'] = 'field_website_logo';
  $handler->display->display_options['fields']['field_website_logo']['label'] = '';
  $handler->display->display_options['fields']['field_website_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_website_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_website_logo']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '[title]<i class="icon-chevron-right"></i>';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['empty'] = 'No website for this client';
  $handler->display->display_options['fields']['title']['empty_zero'] = TRUE;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
  /* Field: Content: Client */
  $handler->display->display_options['fields']['field_website_client']['id'] = 'field_website_client';
  $handler->display->display_options['fields']['field_website_client']['table'] = 'field_data_field_website_client';
  $handler->display->display_options['fields']['field_website_client']['field'] = 'field_website_client';
  $handler->display->display_options['fields']['field_website_client']['label'] = '';
  $handler->display->display_options['fields']['field_website_client']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_website_client']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_website_client']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_website_client']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['path'] = 'websites';

  /* Display: Clients Websites Block */
  $handler = $view->new_display('block', 'Clients Websites Block', 'clients_websites_block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_website_client',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['style_options']['row_class_special'] = FALSE;
  $handler->display->display_options['style_options']['class'] = 'nav nav-list bs-docs-sidenav nav-stacked';
  $handler->display->display_options['style_options']['wrapper_class'] = 'client-sites';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['default_field_elements'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No access to any websites.';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<span>You have no access to any websites.</span>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['block_description'] = 'Clients Websites Block';
  $export['clients'] = $view;

  return $export;
}
