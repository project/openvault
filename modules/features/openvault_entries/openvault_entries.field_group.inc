<?php
/**
 * @file
 * openvault_entries.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function openvault_entries_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_access|node|website|form';
  $field_group->group_name = 'group_access';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'website';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Access',
    'weight' => '1',
    'children' => array(
      0 => 'field_website_accessors',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'label' => 'Access',
      'instance_settings' => array(
        'classes' => 'well',
      ),
    ),
  );
  $export['group_access|node|website|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_audience|node|client|form';
  $field_group->group_name = 'group_audience';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'client';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Audience',
    'weight' => '4',
    'children' => array(
      0 => 'og_group_ref',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'label' => 'Audience',
      'instance_settings' => array(
        'classes' => 'well',
      ),
    ),
  );
  $export['group_audience|node|client|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_audience|node|website|form';
  $field_group->group_name = 'group_audience';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'website';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Audience',
    'weight' => '2',
    'children' => array(
      0 => 'og_group_ref',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'label' => 'Audience',
      'instance_settings' => array(
        'classes' => 'well',
      ),
    ),
  );
  $export['group_audience|node|website|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basic_information|node|client|form';
  $field_group->group_name = 'group_basic_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'client';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic Information',
    'weight' => '0',
    'children' => array(
      0 => 'field_client_email',
      1 => 'field_client_fax',
      2 => 'field_client_logo',
      3 => 'field_client_phone',
      4 => 'title',
      5 => 'path',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'label' => 'Basic Information',
      'instance_settings' => array(
        'classes' => 'well',
      ),
    ),
  );
  $export['group_basic_information|node|client|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basic_information|node|website|default';
  $field_group->group_name = 'group_basic_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'website';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_website_client',
      1 => 'field_website_url',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'label' => 'Basic Information',
      'instance_settings' => array(
        'classes' => 'well',
      ),
    ),
  );
  $export['group_basic_information|node|website|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basic_information|node|website|form';
  $field_group->group_name = 'group_basic_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'website';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic Information',
    'weight' => '0',
    'children' => array(
      0 => 'field_website_client',
      1 => 'field_website_logo',
      2 => 'field_website_url',
      3 => 'title',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'label' => 'Basic Information',
      'instance_settings' => array(
        'classes' => 'well',
      ),
    ),
  );
  $export['group_basic_information|node|website|form'] = $field_group;

  return $export;
}
