<?php
/**
 * @file
 * openvault_entries.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function openvault_entries_default_rules_configuration() {
  $items = array();
  $items['rules_anonymous_access'] = entity_import('rules_config', '{ "rules_anonymous_access" : {
      "LABEL" : "Anonymous Access",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "access" ],
      "REQUIRES" : [ "rules", "path" ],
      "ON" : [ "init" ],
      "IF" : [
        { "user_has_role" : {
            "account" : [ "site:current-user" ],
            "roles" : { "value" : { "1" : "1" } }
          }
        },
        { "path_has_alias" : { "source" : [ "site:login-url" ] } }
      ],
      "DO" : [ { "redirect" : { "url" : [ "site:login-url" ], "force" : 0 } } ]
    }
  }');
  return $items;
}
