<?php

include_once 'template.inc.php';

function vaultstrap_preprocess_html(&$variables) {
  _vaultstrap_preprocess_html($variables);

  //Here goes your code.
}

/**
 * Main top menu
 * #RA To change the render style to work with twitter bootstrap.
 * @param type $variables
 * @return string 
 */
function vaultstrap_links__system_main_menu($variables) {

  $links = $variables['links'];
  $current_link = current_path();

  $output = '';
  if (count($links) > 0) {
    $output .= '<ul class="nav">';
    foreach ($links as $link) {
      if ($current_link == $link['href']) {
        $output .= '<li class="active">';
      }
      else if ($current_link == 'dashboard' && $link['href'] == "<front>") {
        $output .= '<li class="active">';
      }
      else {
        $output .= '<li>';
      }

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link);
      }
      else {
        $output .= l($link['title'], $link['href'], '#');
      }

      $output .= "</li>";
    }
    $output .= '</ul>';
  }

  return $output;
}

/**
 * User secoundary menu
 * #RA To change the render style to work with twitter bootstrap.
 * @global type $language_url
 * @param type $variables
 * @return string 
 */
function vaultstrap_links__system_secondary_menu($variables) {
  $links = $variables['links'];
  global $user;
  $output = '';
  if (count($links) > 0) {
    $output = '';
    $output .= '<ul class="nav pull-right">' . PHP_EOL;
    $output .= '  <li class="divider-vertical"></li>' . PHP_EOL;
    $output .= '  <li class="dropdown">' . PHP_EOL;
    $output .= '    <a data-toggle="dropdown" class="dropdown-toggle" href="#">' . t('User') . ' <b class="caret"></b></a>' . PHP_EOL;
    $output .= '    <ul class="dropdown-menu">' . PHP_EOL;

    foreach ($links as $link) {
      $output .= '<li>';
      if (isset($link['href'])) {
        if ($link['title'] == 'User') {
          $output .= l($user->mail, $link['href'], $link);
        }
        else {
          $output .= l($link['title'], $link['href'], $link);
        }
      }
      else {
        if ($link['title'] == 'User') {
          $output .= l($user->mail, $link['href'], '#');
        }
        else {
          $output .= l($link['title'], $link['href'], '#');
        }
      }
      $output .= '</li>' . PHP_EOL;
    }
    $output .= '    </ul>' . PHP_EOL;
    $output .= '  </li>' . PHP_EOL;
    $output .= '</ul>' . PHP_EOL;
  }
  return $output;
}


function vaultstrap_preprocess_page(&$vars) {
  if (isset($vars['node'])) {
    $suggest = "page__" . $vars['node']->type;
    $vars['theme_hook_suggestions'][] = $suggest;
  }
}

/**
 * Override theme_breadrumb().
 * #RA add node title to the breadcrumb
 * Print breadcrumbs as a list, with separators.
 */
function vaultstrap_breadcrumb($variables) { 
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    $breadcrumbs = '<ul class="breadcrumb">';
    
    $count = count($breadcrumb) - 1;
    foreach ($breadcrumb as $key => $value) {
      if ($count != $key) {
        $breadcrumbs .= '<li>' . $value . '<span class="divider">/</span></li>';
      }
      else{
        $node_title = drupal_get_title();
        if (isset($node_title) && $node_title != '') {
          $breadcrumbs .= '<li>' . $value . '<span class="divider">/</span></li>'; 
          $breadcrumbs .= '<li>' . $node_title . '</li>';
        }
        else {
          $breadcrumbs .= '<li>' . $value . '</li>'; 
        }
      }
    }
  
    $breadcrumbs .= '</ul>';
    
    return $breadcrumbs;
  }
}
