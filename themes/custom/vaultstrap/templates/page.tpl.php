<?php
  global $user;
  global $front_page;
?>
<div class="container">
<header role="banner" id="page-header">
  <?php if ($user->uid > 0): ?>  
  <div class="container">   
   <div class="navbar navbar-inverse">
          <div class="navbar-inner">
            <div class="container">
              <button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <?php if (variable_get('site_name')): ?>
                <a href="<?php print $front_page; ?>" class="brand" id="site-home"><?php print variable_get('site_name'); ?></a>
              <?php endif; ?>        

                
          <div class="nav-collapse collapse navbar-responsive-collapse">
          <?php if ($main_menu): ?>
              <?php print theme('links__system_main_menu', array(
                'links' => $main_menu,
                'attributes' => array(
                  'id' => 'main-menu-links',
                  'class' => array('links', 'clearfix'),
                ),
                'heading' => array(
                  'text' => t('Main menu'),
                  'level' => 'h2',
                  'class' => array('element-invisible'),
                ),
              )); ?>
          <?php endif; ?>
              <button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
          
        <?php if ($secondary_menu): ?>
             <?php  print theme('links__system_secondary_menu', array(
                'links' => $secondary_menu,
                'attributes' => array(
                'id' => 'secondary-menu-links',
                'class' => array('links', 'clearfix'),
              ),
              'heading' => array(
                'text' => t('Secondary menu'),
                'level' => 'h2',
                'class' => array('element-invisible'),
              ),
            )); ?>
        <?php endif; ?>
          
        <button class="btn btn-navbar" data-target=".nav-collapse" data-toggle="collapse" type="button">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
          
        <?php  print render($page['header']); ?>

        
      </div> <!-- .nav-collapse -->
    </div> <!-- .container -->
  </div> <!-- .navbar-inner -->
 </div> <!-- .avbar navbar-inverse -->
</div> <!-- /.section, /#header -->

</header> <!-- /#header -->

<?php if ($breadcrumb): print $breadcrumb; endif;?>

<?php endif; ?>
  <div class="row">  
    <?php if ($page['sidebar_first']): ?>
      <aside class="span3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>  
    <section class="<?php print _bootstrap_content_span($columns); ?>">  
      <?php if ($page['highlighted']): ?>
        <div class="highlighted hero-unit"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="page-header"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if ($tabs): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if ($page['help']): ?> 
        <div class="well"><?php print render($page['help']); ?></div>
      <?php endif; ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if ($page['sidebar_second']): ?>
      <aside class="span3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>
  </div>

  <footer class="footer container">
    <?php print render($page['footer']); ?>
  </footer>
</div>