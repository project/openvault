<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */

 // #RA to get the current active item
  $path = drupal_get_normal_path($_GET['q']); //get alias of URL
  $path = explode('/', $path); //break path into an array
  
  $active_path = '';
  if (count($path)> 1) {
    $active_path = $path[1];
  }

?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php print $list_type_prefix;?>
    <?php foreach ($rows as $id => $row):
        $active_list_item =  $view->result[$id]->nid;
        if ($active_list_item == $active_path) {
          if ($classes_array[$id] != "") {
            $classes_array[$id] .= " active";
          }
          else {
            $classes_array[$id] .= "active"; 
          }
        }
      ?>
      <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  <?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>