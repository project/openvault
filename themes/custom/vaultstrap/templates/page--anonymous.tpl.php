<div class="container-anonymouse">
  <header role="banner" id="page-header-anonymouse">
    <?php if ($logo): ?>
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
    <?php endif; ?>

    <?php if ($site_name || $site_slogan): ?>
      <div id="name-and-slogan">
        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name">
              <strong>
                <span><?php print $site_name; ?></span>
              </strong>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <<span><?php print $site_name; ?></span>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id="site-slogan">
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>

      </div> <!-- /#name-and-slogan -->
    <?php endif; ?>
  </header>
   <h3><?php print t('Sign in with your credentials'); ?></h3>
    <?php if ($page['highlighted']): ?>
      <div class="highlighted hero-unit"><?php print render($page['highlighted']); ?></div>
    <?php endif; ?>

    <?php print $messages; ?>
    <?php if ($page['help']): ?> 
      <div class="well">
        <?php print render($page['help']); ?>
      </div>
    <?php endif; ?>
    <?php if ($action_links): ?>
      <ul class="action-links"><?php print render($action_links); ?></ul>
    <?php endif; ?>
  <div class="row hero-unit">   
    <section class="<?php print _bootstrap_content_span($columns); ?>">  
      <?php print render($page['content']); ?>
    </section>
  </div>
  <footer class="footer container">
    <?php print render($page['footer']); ?>
  </footer>
</div>